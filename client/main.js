import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {copyToClipboard } from '../imports/copyToClipboard.js';

urlCollection = new Mongo.Collection('urls');
Meteor.subscribe('userURL');

/*Known problems
    Well for a start it only works on the localhost because it relies on Meteor.absoluteUrl(), so testing from a different machine results in errors.
    Happily enough, the redirects still work (i.e., localhost:3000/abcdef doesn't work, but 192.168.0.5:3000/abcdef does)

    Having never used Meteor before I'm unsure how to actually write test cases for it and I ran out of time before I was able to read the docs about it. However, it seems to be mostly stable.

*/

Template.addURL.events({
    'submit form': function(event){
        //we don't do much validation here (or any as the case may be)
        event.preventDefault();
        var longURL = event.target.url.value;
        Meteor.call('addURL',longURL,function(err,res){
            //callback function to run when the server creates a new record and confirms its uniqueness
            if(typeof err === "undefined"){
                Session.set("urlCreated",true);
                Session.set("urlCreatedId",res);
            }else{
                console.log(err);
            }
        })
        event.target.url.value="";
    }
});


Template.addedURL.helpers({
    'urlCreated':function(){
        return( Session.get("urlCreated")===true);
    },
    'docCreated': function(){
        let doc = urlCollection.findOne({_id:Session.get("urlCreatedId")});
        //if it can't find a document to display, either one hasn't been created or this user doesn't have access
        //this will also occur when logging in/out
        if(typeof doc === "undefined"){
            Session.set("urlCreated",false);
            return;
        }
        //fill out some variables for display
        doc.short = Meteor.absoluteUrl(doc.short);
        doc.longLength = doc.long.length;
        doc.shortLength = doc.short.length;
        doc.ratio = Math.round(100 * (doc.longLength - doc.shortLength) / doc.longLength);
        return doc;
    }
})



Template.addedURL.events({
    'click div':function(){
        //because this div isn't associated with a data source need to find the correct one manually
        copyToClipboard(Meteor.absoluteUrl(urlCollection.findOne({_id:Session.get("urlCreatedId")}).short));
    }
});



Template.listURL.events({
    'click span':function(){
        //don't need to use absoluteUrl because it is already added in the list helper
        copyToClipboard(this.short);
    }
});

Template.listURL.helpers({
    //list of URLs this user has shortened
    'userURLs':function () {
        return urlCollection.find({createdBy:Meteor.userId()}).fetch().map( (doc)=> {
            //if long url is too long, concatenate it
            doc.concat = doc.long.length> 63
                ? doc.long.slice(0,60) + "..."
                : doc.long;
            //fix short to display full link
            doc.short = Meteor.absoluteUrl(doc.short);
            return doc;
        });
    },
    'hasURLs':function(){
        //test if the user has any previous URLs saved
        return urlCollection.findOne({createdBy:Meteor.userId()});
    }

});
