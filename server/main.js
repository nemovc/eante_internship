import { Meteor } from 'meteor/meteor';

urlCollection = new Mongo.Collection('urls');

Meteor.publish('userURL', function(){
    var currentUserId = this.userId;
    //only allow the client to find redirections they have created
    return urlCollection.find({ createdBy: currentUserId });
    //doesn't affect actual redirection as that is done on the server

});




Meteor.startup(() => {


});

Meteor.methods({
    'addURL':function(url){
        //we're not doing any fancy checking here - assume that so long as it's a non-empty string the user has posted a valid URL
        check(url, Match.Where(function (str){
            check(str, String);
            return str.length > 0;
        }));

        // null if not logged in, but this is acceptable to submit without user
        var currentUserId = Meteor.userId();

        while(true){
            //find an acceptable short
            //shouldn't just use a portion of the _id because without checking there may be collisions
            //this implementation is very naive and will take exponentially longer to run as the keyspace fills up
            //but will still loop <10 times with 99.99% prob when the keyspace has 28.4b redirects

            var short = Random.id(6);
            let found = urlCollection.findOne({short:short});
            if(!(found)){
                break;
            }
        }
        // insert and return new document ID for client callback
        let newDoc = urlCollection.insert({
            short:short,
            long:url,
            createdBy:currentUserId
        })
        return newDoc;
    }
})

//deal with actual redirect
WebApp.connectHandlers.use('/',(req,res,next)=>{
    //remove leading backslash
    let targ = req.url.slice(1);
    let doc = urlCollection.findOne({short:targ});
    if(doc){
        // if a doc is found, then this address is a valid redirect
        res.setHeader("Location", doc.long);
        res.writeHead(301);
        res.end();
    }else{
        //if no doc found, its either an invalid redirect or /, and in either case should launch the meteor client
        next();
    }
});
